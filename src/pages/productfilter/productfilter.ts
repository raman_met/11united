import { Component } from '@angular/core';
import { NavController, NavParams, Events, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

@Component({
  selector: 'page-productfilter',
  templateUrl: 'productfilter.html'
})
export class ProductfilterPage {
  
  data: any;
  colorData: any = [];
  sizeData: any = [];
  minPrice:any;
  maxPrice:any;
  price: any = { lower: 0, upper: 100 };
  selectedcolorID:any = '';
  selectedsizeID:any = '';

  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    public storage: Storage,
    public authService: AuthServiceProvider,
    public navParams: NavParams, ) {
      
  }

  ionViewWillEnter(){
    this.authService.showLoader('Please wait...');
    this.authService.getData("ws-categoryfilterlist.php").then(result => {
      this.data = result;
      this.colorData = this.data.result.color_list;
      this.sizeData = this.data.result.size_list;
      this.minPrice = this.data.result.minimum_price;
      this.maxPrice = this.data.result.maximum_price;
      this.price = { lower: this.minPrice, upper: this.maxPrice };
      console.log("categoryfilterlist: ", this.data);
      this.authService.hideLoader();
    },
      error => {
        this.authService.hideLoader();
        console.log(error);
      });
  }

  onColorChanged(val){
    this.selectedcolorID = val;
  }

  onSizeChanged(val){
    this.selectedsizeID = val;
  }

  onDone() {
    this.viewCtrl.dismiss({minprice:this.price.lower, maxprice:this.price.upper, sizeID:this.selectedsizeID, colorID:this.selectedcolorID});
  }

  onClick(){
    this.viewCtrl.dismiss({minprice:'', maxprice:'', sizeID:'', colorID:''});
  }

}
