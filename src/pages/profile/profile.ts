import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, Events } from 'ionic-angular';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { EditprofilePage } from '../editprofile/editprofile';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { CartPage } from '../cart/cart';
import { WishlistPage } from '../wishlist/wishlist';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  data: any;
  userID: any;
  userDetails: any;
  email: any;
  mobile: any;
  cartCount:any = 0;
  wishCount:any = 0;

  constructor(public navCtrl: NavController,
    public events: Events, 
    public storage: Storage,
    public authService: AuthServiceProvider,
    public navParams: NavParams) {
      this.storage.get('userDetails').then((result) => {
        this.data = result;
        this.userID = result.ID;
        console.log("userID: ", this.userID);
        this.initilizeData();
      });
      this.events.subscribe('cart:count', (eventData) => {
        console.log(eventData);
        this.cartCount = eventData;
      }); 
      this.events.subscribe('wish:count', (eventData) => {
        //console.log(eventData);
        this.wishCount = eventData;
      }); 
  }

  initilizeData(){
    this.authService.showLoader('Please wait...');
      var body = "user_id=" + this.userID;
      this.authService.postData("ws-personal-info.php", body).then(result => {
        this.data = result;        
        this.userDetails = this.data.result.personal_detail[0];
        this.authService.hideLoader();
        this.email = this.userDetails.user_email;
        this.mobile = this.userDetails.mobile;
        console.log("userDetails: ", this.userDetails);
      },
      error => {
        this.authService.hideLoader();
        console.log(error);
      });
  }

  onChangePassword(){
    this.navCtrl.push(ChangepasswordPage);
  }

  onEditProfile(){
    this.navCtrl.push(EditprofilePage);
  }

  oncart(){
    this.navCtrl.push(CartPage);
  }

  onwhishlist(){
    this.navCtrl.push(WishlistPage);
  }

}
