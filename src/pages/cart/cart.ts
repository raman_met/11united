import { Component } from '@angular/core';
import { NavController, NavParams, Events, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';
import { ProductdetailsPage } from '../productdetails/productdetails';
import { MyaccountPage } from '../myaccount/myaccount';
import { ProfilePage } from '../profile/profile';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { WishlistPage } from '../wishlist/wishlist';
import { NewaddressPage } from '../newAddress/newaddress';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { ThankyouPage } from '../thankyou/thankyou';

@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html'
})
export class CartPage {

  userID: any;
  userName: any;
  userDetails: any;
  data: any;
  cartCount: any = 0;
  cartData: any = [];
  cartAmount: any = [];
  wishCount: any = 0;
  fname: any;
  lname: any;
  landmark: any = '';
  postcode: any;
  city: any;
  state: any;
  phone: any;
  address_type: any;
  sfname: any;
  slname: any;
  slandmark: any = '';
  spostcode: any;
  scity: any;
  sstate: any;
  sphone: any;
  saddress_type: any;
  paypalData: any;
  isDisabled: boolean = false;
  quantity: any;
  alert_Ctrl: any;
  same_address: boolean = false;

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    private payPal: PayPal,
    public events: Events,
    public storage: Storage,
    public navParams: NavParams,
    public authService: AuthServiceProvider, ) {
    this.events.subscribe('cart:count', (eventData) => {
      //console.log(eventData);
      this.cartCount = eventData;
    });
    this.events.subscribe('wish:count', (eventData) => {
      //console.log(eventData);
      this.wishCount = eventData;
    });
    events.subscribe('billing_address:added', (data) => {
      this.getBillingAddressDetails();
    });
    events.subscribe('shipping_address:added', (data) => {
      this.getBillingAddressDetails();
    });
    this.getBillingAddressDetails();
  }

  getBillingAddressDetails() {
    this.storage.get('userDetails').then((result) => {
      this.data = result;
      this.userID = result.ID;
      console.log("userID: ", this.userID);
      this.authService.showLoader('Please wait...');
      var body = "user_id=" + this.userID;
      this.authService.postData("ws-show-address.php", body).then(result => {
        this.data = result;
        console.log("Billing address: ", this.data);
        this.landmark = this.data.billing_address;
        this.address_type = this.data.billing_address_type;
        this.city = this.data.billing_city;
        this.fname = this.data.billing_first_name;
        this.lname = this.data.billing_last_name;
        this.phone = this.data.billing_phone;
        this.postcode = this.data.billing_postcode;
        this.state = this.data.billing_state;
        //this.authService.hideLoader();
        this.getShippingAddressDetails();
      },
        error => {
          this.authService.hideLoader();
          console.log(error);
        });
    });
  }

  getShippingAddressDetails() {
    this.storage.get('userDetails').then((result) => {
      this.data = result;
      this.userID = result.ID;
      console.log("userID: ", this.userID);
      //this.authService.showLoader('Please wait...');
      var body = "user_id=" + this.userID;
      this.authService.postData("ws-show-shipping-address.php", body).then(result => {
        this.data = result;
        console.log("Shipping address: ", this.data);
        this.slandmark = this.data.shipping_address;
        this.saddress_type = this.data.shipping_address_type;
        this.scity = this.data.shipping_city;
        this.sfname = this.data.shipping_first_name;
        this.slname = this.data.shipping_last_name;
        this.sphone = this.data.shipping_phone;
        this.spostcode = this.data.shipping_postcode;
        this.sstate = this.data.shipping_state;
        //this.authService.hideLoader();
        this.getallData();
      },
        error => {
          this.authService.hideLoader();
          console.log(error);
        });
    });
  }

  stateChange() {
    if (this.same_address == true) {
      this.storage.get('userDetails').then((result) => {
        this.data = result;
        this.userID = result.ID;
        console.log("userID: ", this.userID);
        this.authService.showLoader('Please wait...');
        var body = "user_id=" + this.userID;
        this.authService.postData("ws-show-address.php", body).then(result => {
          this.data = result;
          console.log("Billing address: ", this.data);
          this.slandmark = this.data.billing_address;
          this.saddress_type = this.data.billing_address_type;
          this.scity = this.data.billing_city;
          this.sfname = this.data.billing_first_name;
          this.slname = this.data.billing_last_name;
          this.sphone = this.data.billing_phone;
          this.spostcode = this.data.billing_postcode;
          this.sstate = this.data.billing_state;
          this.authService.hideLoader();
        },
          error => {
            this.authService.hideLoader();
            console.log(error);
          });
      });
    } else {
      this.storage.get('userDetails').then((result) => {
        this.data = result;
        this.userID = result.ID;
        console.log("userID: ", this.userID);
        this.authService.showLoader('Please wait...');
        var body = "user_id=" + this.userID;
        this.authService.postData("ws-show-shipping-address.php", body).then(result => {
          this.data = result;
          console.log("Shipping address: ", this.data);
          this.slandmark = this.data.shipping_address;
          this.saddress_type = this.data.shipping_address_type;
          this.scity = this.data.shipping_city;
          this.sfname = this.data.shipping_first_name;
          this.slname = this.data.shipping_last_name;
          this.sphone = this.data.shipping_phone;
          this.spostcode = this.data.shipping_postcode;
          this.sstate = this.data.shipping_state;
          this.authService.hideLoader();
        },
          error => {
            this.authService.hideLoader();
            console.log(error);
          });
      });
    }
  }

  getallData() {
    this.storage.get('userDetails').then((result) => {
      this.data = result;
      this.userID = result.ID;
      console.log("userDetails: ", this.data);
      var body = "user_id=" + this.userID;
      this.authService.postData("ws-cartlist.php", body).then(result => {
        this.data = result;
        this.cartData = this.data.result.list;
        this.cartAmount = this.data.result;
        console.log("cartData: ", this.cartData);
        this.authService.hideLoader();
      },
        error => {
          this.authService.hideLoader();
          console.log(error);
        });
    });
  }

  onNewAddressBook(value) {
    console.log(value);
    if (value == 'Billing') {
      var params = { addressType: value, landmark: this.landmark, address_type: this.address_type, city: this.city, phone: this.phone, postcode: this.postcode, state: this.state };
    } else {
      var params = { addressType: value, landmark: this.slandmark, address_type: this.saddress_type, city: this.scity, phone: this.sphone, postcode: this.spostcode, state: this.sstate };
    }
    this.navCtrl.push(NewaddressPage, params);
  }

  onDeleteCart(cartID) {
    console.log("Cart ID: ", cartID);
    this.alert_Ctrl = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Are you sure you want to delete this item from Cart?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            setTimeout(() => {
              this.alert_Ctrl.dismiss();
            }, 1000);
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.authService.showLoader('Please wait...');
            var body = "user_id=" + this.userID + "&cart_id=" + cartID;
            this.authService.postData("ws-deletecartitem.php", body).then(result => {
              this.data = result;
              console.log("Delete Cart: ", this.data);
              //this.authService.showLoader('Please wait...');
              this.getallData();
            },
              error => {
                this.authService.hideLoader();
                console.log(error);
              });
          }
        }
      ]
    });
    this.alert_Ctrl.present();
  }

  onUpdateCart(proID, qty) {
    console.log("Product ID: ", proID);
    if (String(qty).trim() == '' || String(qty).trim() == '0') {
      this.authService.presentToast('Please enter Quantity')
    } else {
      this.authService.showLoader('Please wait...');
      var body = "user_id=" + this.userID + "&product_id=" + proID + "&qty=" + qty;
      this.authService.postData("ws-updatecartitem.php", body).then(result => {
        this.data = result;
        console.log("Update Cart: ", this.data);
        //this.authService.showLoader('Please wait...');
        this.getallData();
      },
        error => {
          this.authService.hideLoader();
          console.log(error);
        });
    }
  }

  addtowishlist(prodID) {
    //ws-addtowishlist.php
    this.authService.showLoader('Please wait...');
    var body = "user_id=" + this.userID + "&product_ids=" + prodID;
    this.authService.postData("ws-addtowishlist.php", body).then(result => {
      this.data = result;
      console.log("addtowishlist : ", this.data);
      this.authService.hideLoader();
      this.navCtrl.push(WishlistPage);
    },
      error => {
        this.authService.hideLoader();
        console.log(error);
      });
  }

  onPlaceorder() {
    if (this.slandmark == '') {
      this.authService.presentToast("Please add Shipping Address");
    } else if (this.landmark == '') {
      this.authService.presentToast("Please add Billing Address");
    } else if (String(this.quantity).trim() == '' || String(this.quantity).trim() == '0') {
      this.authService.presentToast("Please enter Quantity");
    } else {
      this.authService.showLoader('Please wait...');
      this.storage.get('userDetails').then((result) => {
        this.data = result;
        this.userID = this.data.ID;
        this.userName = this.data.user_first_name + " " + this.data.user_last_name;
        this.payPal.init({
          PayPalEnvironmentProduction: 'YOUR_PRODUCTION_CLIENT_ID',
          PayPalEnvironmentSandbox: 'ASR6nYwzGPSbQBQcqGVtN6HH_jSMBmscYvSv8vOAjk8mqW04-6iu0OOw4E1NE0tLpSn55inNHMlF7WOB'
        }).then(() => {
          this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
          })).then(() => {
            let payment = new PayPalPayment((this.cartAmount.order_final_total).toString(), 'USD', 'Description', 'sale');
            this.payPal.renderSinglePaymentUI(payment).then((response) => {
              console.log(response);
              this.paypalData = response;
              var body = "user_id=" + this.userID +
                "&transaction_id=" + this.paypalData.response.id +
                "&order_tax=" + this.cartAmount.estimated_gst +
                "&order_total=" + this.cartAmount.order_final_total +
                "&paypal_address=" +
                "&payer_name=" + this.userName +
                "&payment_type=paypal" +
                "&payment_status=" + this.paypalData.response.state +
                "&paypal_transaction_fee=0";
              this.authService.postData("ws-saveorder.php", body).then(result => {
                this.data = result;
                this.authService.hideLoader();
                if (this.data.status.error_code == 0) {
                  this.navCtrl.push(ThankyouPage);
                  console.log(this.data);
                } else {
                  console.log(this.data.status.message);
                }
              },
                error => {
                  this.authService.hideLoader();
                  console.log(error);
                });
            }, () => {

            });
          }, () => {

          });
        }, () => {

        });
      });
    }
  }

  onwhishlist() {
    this.navCtrl.push(WishlistPage);
  }

  onQuantityChange(val) {
    this.quantity = val;
    console.log(val);
    /* if(val != null && val > 0 ){
      this.isDisabled = false;
    } else {
      this.isDisabled = true;
    } */
  }

}
