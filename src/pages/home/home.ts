import { Component } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { CatagoriesPage } from '../catagories/catagories';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ProductPage } from '../product/product';
import { CartPage } from '../cart/cart';
import { SearchPage } from '../search/search';
import { ProductdetailsPage } from '../productdetails/productdetails';
import { WishlistPage } from '../wishlist/wishlist';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  data: any;
  bannerData: any = [];
  categoryData: any = [];
  hotData: any = [];
  newwArrivalsData: any = [];
  subcategoryData: any = [];
  saleBannerData: any = [];
  cartCount:any = 0;
  wishCount:any = 0;

  constructor(public navCtrl: NavController,
    public events: Events,
    public authService: AuthServiceProvider,) {
      this.events.subscribe('cart:count', (eventData) => {
        //console.log(eventData);
        this.cartCount = eventData;
      }); 
      this.events.subscribe('wish:count', (eventData) => {
        //console.log(eventData);
        this.wishCount = eventData;
      });      
  }

  ionViewWillEnter() {
    this.authService.showLoader('Please wait...');
    this.authService.getData("ws-homepage.php").then(result => {
      this.data = result;
      console.log("categoryData: ", this.data);
      this.bannerData = this.data.banner_url;
      this.categoryData = this.data.category_list;
      this.subcategoryData = this.data.cat_slider;
      this.hotData = this.data.hot_deals;
      this.newwArrivalsData = this.data.new_arrival;
      this.saleBannerData = this.data.sale_banner;
      console.log("subcategoryData: ", this.subcategoryData);
      this.authService.hideLoader();
    },
    error => {
      this.authService.hideLoader();
      console.log(error);
    });
  }

  categoryImgClass(name){
    var catbg;
    if(name == 'Fashion'){
      catbg = 'fashion_category_sec'
      return catbg
    } else if (name == 'Sports') {
      catbg = 'sports_category_sec'
      return catbg
    } else {
      catbg = 'wellness_category_sec'
      return catbg
    }
  }

  onProductList(catID, catName){
    console.log("catName: ", catID);
    this.navCtrl.push(ProductPage, {catID: catID, catName: catName});
  }

  onProductdetails(pro_id){
    this.navCtrl.push(ProductdetailsPage, {proID: pro_id});
  }

  oncart(){
    this.navCtrl.push(CartPage);
  }

  onwhishlist(){
    this.navCtrl.push(WishlistPage);
  }

  onCatagory(){
    this.navCtrl.push(CatagoriesPage);
  }

  onSearch(){
    this.navCtrl.push(SearchPage);
  }

}
