import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { HomePage } from '../home/home';
import { CartPage } from '../cart/cart';
import { Storage } from '@ionic/storage';
import { NewaddressPage } from '../newAddress/newaddress';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { WishlistPage } from '../wishlist/wishlist';

@Component({
  selector: 'page-billingaddressbook',
  templateUrl: 'billingaddressbook.html'
})
export class BillingaddressbookPage {

  data: any;
  userID: any;
  fname: any;
  lname: any;
  landmark: any = '';
  postcode: any;
  city: any;
  state: any;
  phone: any;
  address_type: any;
  cartCount:any = 0;
  wishCount:any = 0;

  constructor(public navCtrl: NavController,
    public authService: AuthServiceProvider,
    public events: Events,
    public storage: Storage,
    public navParams: NavParams) {
      this.storage.get('userDetails').then((result) => {
        this.data = result;
        console.log("userDetails: ", this.data);
        this.userID = this.data.ID;
        this.getAddressDetails();
      });
      events.subscribe('billing_address:added', (data) => {
        this.getAddressDetails();
      });
      this.events.subscribe('cart:count', (eventData) => {
        //console.log(eventData);
        this.cartCount = eventData;
      }); 
      this.events.subscribe('wish:count', (eventData) => {
        //console.log(eventData);
        this.wishCount = eventData;
      });
  }

  getAddressDetails(){
    this.authService.showLoader('Please wait...');
    var body = "user_id=" + this.userID;
    this.authService.postData("ws-show-address.php", body).then(result => {
      this.data = result;
      console.log("address: ", this.data);
      this.landmark = this.data.billing_address;
      this.address_type = this.data.billing_address_type;
      this.city = this.data.billing_city;
      this.fname = this.data.billing_first_name;
      this.lname = this.data.billing_last_name;
      this.phone = this.data.billing_phone;
      this.postcode = this.data.billing_postcode;
      this.state = this.data.billing_state;
      this.authService.hideLoader();
    },
    error => {
      this.authService.hideLoader();
      console.log(error);
    });
  }

  onNewAddressBook(value){
    console.log(value);
    //this.navCtrl.push(NewaddressPage, {addressType: value});
    this.navCtrl.push(NewaddressPage, {addressType: value, landmark: this.landmark, address_type: this.address_type, city: this.city, phone: this.phone, postcode: this.postcode, state: this.state});
  }

  oncart(){
    this.navCtrl.push(CartPage);
  }

  onwhishlist(){
    this.navCtrl.push(WishlistPage);
  }

}
