import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

@Component({
  selector: 'page-changepassword',
  templateUrl: 'changepassword.html'
})
export class ChangepasswordPage {
  isDisabled:boolean = false;
  oldPass:any = '';
  newPass:any = '';
  cnewPass:any = '';
  data:any;
  userID: any;

  constructor(public storage: Storage,
    public navCtrl: NavController,
    public authService: AuthServiceProvider,
    public toastCtrl:ToastController,) {
      this.storage.get('userDetails').then((result) => {
        this.data = result;
        this.userID = result.ID;
        console.log("userID: ", this.userID);
      }); 
  }

  onChangePassword(){
    //this.navCtrl.push(SignupPage);
    if(this.oldPass.trim() == ''){
      this.presentToast('Please enter your Old Password');
    } else if(this.newPass.trim() == ''){
      this.presentToast('Please enter New Password');
    } else if(this.cnewPass.trim() == ''){
      this.presentToast('Please re-enter New Password');
    } else if(this.newPass.trim() != this.cnewPass.trim()){
      this.presentToast('Password Mismatch');
    } else {
      this.authService.showLoader('Please wait...');
      var body="user_id="+ this.userID + "&old_password=" + this.oldPass + "&new_password=" + this.newPass;
      this.authService.postData("ws-changepassword.php", body).then(result => { 
        console.log(result);
        this.data = result;
        this.authService.hideLoader();
        if(this.data.status.error_code == 0){
          this.presentToast(this.data.status.message);
          this.navCtrl.pop();
        }else{
          this.presentToast(this.data.status.message)
        }
      }, 
      error => {
        this.authService.hideLoader();
        console.log(error);
      });
    }
  }

  presentToast(msg) {
    this.isDisabled = true;
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
      this.isDisabled = false;    
    });
    toast.present();
  }

}
