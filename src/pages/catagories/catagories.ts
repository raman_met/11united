import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ProductPage } from '../product/product';

@Component({
  selector: 'page-catagories',
  templateUrl: 'catagories.html'
})
export class CatagoriesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    
  }

  onProduct(){
    this.navCtrl.push(ProductPage);
  }

}
