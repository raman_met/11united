import { Component } from '@angular/core';
import { NavController, NavParams, Events, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { CartPage } from '../cart/cart';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { WishlistPage } from '../wishlist/wishlist';
import { AddreviewPage } from '../addreview/addreview';
import { ProductreviewPage } from '../productreview/productreview';
import { SocialSharing } from '@ionic-native/social-sharing';

@Component({
  selector: 'page-productdetails',
  templateUrl: 'productdetails.html'
})
export class ProductdetailsPage {  
  prodID: any;
  data: any;
  userID: any;
  productDetailsdata:any = [];
  product_Detailsdata:any = [];
  productColor:any = [];
  productSize:any = [];
  bannerImage:any = [];
  reviewData: any = [];
  mycolor: any = null;
  mysize: any = null;
  name: any;
  description: any;
  rating: any;
  price: any;
  prodImg: any;
  cartCount:any = 0;
  productArr:any = [];

  constructor(public navCtrl: NavController,
    public authService: AuthServiceProvider,
    private socialSharing: SocialSharing,
    public events: Events,
    public storage: Storage, 
    public platform: Platform,
    public navParams: NavParams) {
    this.storage.remove('color');
    this.storage.remove('size');
    this.prodID = this.navParams.get('proID');
    console.log("prodID: ", this.prodID);
    this.events.subscribe('cart:count', (eventData) => {
      //console.log(eventData);
      this.cartCount = eventData;
    }); 
    this.events.subscribe('review:added', () => {
      this.getProdDetails();
    });
    
  }

  ionViewWillEnter(){
    this.getProdDetails();
  }

  getProdDetails(){
    this.storage.get('userDetails').then((result) => {
      this.data = result;
      this.userID = result.ID;
      console.log("userID: ", this.userID);
      this.authService.showLoader('Please wait...');
      var body = "user_id=" + this.userID + "&id="+this.prodID;
      this.authService.postData("ws-productdetail.php", body).then(result => {
        this.data = result;        
        this.productDetailsdata = this.data.result.details;

        this.storage.get('productRecentView').then((result: any[]) => {
          this.productArr = result || [];
          this.productArr.push(this.productDetailsdata);
          this.storage.set('productRecentView', this.productArr);
        });

        this.name = this.productDetailsdata.post_title;
        this.description = this.productDetailsdata.Description;
        this.bannerImage = this.productDetailsdata.banner_images;
        this.prodImg = this.productDetailsdata.productimage;
        this.rating = this.productDetailsdata.product_rating;
        this.price = this.productDetailsdata.product_price;
        this.productColor = this.productDetailsdata.variation_color;
        this.productSize = this.productDetailsdata.variation_size;
        console.log("productDetailsdata: ", this.productDetailsdata);
        //this.authService.hideLoader();
        this.getProductReview();
      },
      error => {
        this.authService.hideLoader();
        console.log(error);
      });
    });    
  }
  
  getProductReview(){
    var body = "id="+this.prodID;
    this.authService.postData("ws-product-review-list.php", body).then(result => {
      this.data = result;        
      this.reviewData = this.data.result.review_list;
      console.log("review: ", this.data);
      this.authService.hideLoader();
    },
    error => {
      this.authService.hideLoader();
      console.log(error);
    });
  }

  onSizeChanged(value){
    console.log("onSizeChanged: ", value.toLowerCase());
    this.storage.set("size", value.toLowerCase());
    this.storage.get('color').then((result) => {
      this.mycolor = result;
      this.storage.get('size').then((result) => {
        this.mysize = result;
        console.log(this.mycolor, " ", this.mysize);
        if (this.mycolor != null && this.mysize != null) {
          this.authService.showLoader('Please wait...');
          var body = "size=" + this.mysize + "&color=" + this.mycolor + "&id=" + this.prodID;
          this.authService.postData("ws-prodbycolsize.php", body).then(result => {
            this.data = result;
            console.log('prodbycolsize: ', this.data.result.product_list[0].variation[0]);            
            if (this.data.result.product_list[0].variation[0] != null) {
              this.product_Detailsdata = this.data.result.product_list[0].variation[0];
              this.prodID = this.product_Detailsdata.variationid;
              if(this.product_Detailsdata.regular_price == this.product_Detailsdata.active_price){
                this.price = '<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>' + this.product_Detailsdata.active_price + '</span>';
              } else {
                this.price = '<del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>' + this.product_Detailsdata.regular_price + '</span></del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>' + this.product_Detailsdata.active_price + '</span>';
              }
            }            
            this.authService.hideLoader();
          },
          error => {
            console.log(error);
            this.authService.hideLoader();
          });
        }
      });
    });
  }

  onColorChanged(value){
    console.log("onColorChanged: ", value.toLowerCase());
    this.storage.set("color", value.toLowerCase());
    this.storage.get('size').then((result) => {
      this.mysize = result;
      this.storage.get('color').then((result) => {
        this.mycolor = result;
        console.log(this.mycolor, " ", this.mysize);
        if (this.mycolor != null && this.mysize != null) {
          this.authService.showLoader('Please wait...');
          var body = "size=" + this.mysize + "&color=" + this.mycolor + "&id=" + this.prodID;
          this.authService.postData("ws-prodbycolsize.php", body).then(result => {
            this.data = result;
            console.log('prodbycolsize: ', this.data.result.product_list[0].variation[0]);            
            if (this.data.result.product_list[0].variation[0] != null) {
              this.product_Detailsdata = this.data.result.product_list[0].variation[0];
              this.prodID = this.product_Detailsdata.variationid;
              if(this.product_Detailsdata.regular_price == this.product_Detailsdata.active_price){
                this.price = '<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>' + this.product_Detailsdata.active_price + '</span>';
              } else {
                this.price = '<del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>' + this.product_Detailsdata.regular_price + '</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>' + this.product_Detailsdata.active_price + '</span></ins>';
              }              
            }
            this.authService.hideLoader();
          },
          error => {
            console.log(error);
            this.authService.hideLoader();
          });
        }
      });
    });
  }

  gotoWishlist(){
    this.navCtrl.push(WishlistPage);
  }

  onAddWishlist(prodID){
    this.authService.showLoader('Please wait...');
    var body = "user_id=" + this.userID + "&product_ids="+prodID;
    this.authService.postData("ws-addtowishlist.php", body).then(result => {
      this.data = result;
      console.log("addtowishlist : ", this.data);
      this.authService.hideLoader();
      this.navCtrl.push(WishlistPage);
    },
    error => {
      this.authService.hideLoader();
      console.log(error);
    }); 
  }

  onBuy(){

  }

  addToCart(){
    if ( this.mysize == null && this.productSize != null) {
      this.authService.presentToast('Please select product Size');
    } else if (this.mycolor == null && this.productColor != null){
      this.authService.presentToast('Please select product Color');
    } else {
      this.authService.showLoader('Please wait...');
      var body = "user_id=" + this.userID + "&id="+this.prodID + "&qty=1";
      this.authService.postData("ws-productaddtocart.php", body).then(result => {
        this.data = result;
        console.log("addtocart : ", this.data);
        this.authService.hideLoader();
        this.navCtrl.push(CartPage);
      },
      error => {
        this.authService.hideLoader();
        console.log(error);
      }); 
    }
  }

  formatdate(review_date){
    var newdate;
    newdate = review_date.split(' ');
    return newdate[0];
  }

  onShare(){
    this.socialSharing.share(this.description, this.name, null, 'https://elevenunited.fitser.com/wp-admin/');
  }

  oncart(){
    this.navCtrl.push(CartPage);
  }

  onAddReview(){
    this.navCtrl.push(AddreviewPage, {prodID: this.prodID});
  }

  onAllReview(){
    this.navCtrl.push(ProductreviewPage, {prodID: this.prodID});
  }

}
