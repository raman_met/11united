import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

@Component({
  selector: 'page-forgetpassword',
  templateUrl: 'forgetpassword.html'
})

export class ForgetpasswordPage {
  isDisabled:boolean = false;
  email:any = '';
  data:any;

  constructor(
    public navCtrl: NavController,
    public authService: AuthServiceProvider,
    public toastCtrl:ToastController,) {
  }

  onSendMail(){
    //this.navCtrl.push(SignupPage);
    if(this.email.trim() == ''){
      this.presentToast('Please enter your Email ID');
    } else {
      this.authService.showLoader('Please wait...');
      var body="email="+ this.email;
      this.authService.postData("ws-forgotpassword.php", body).then(result => { 
        console.log(result);
        this.data = result;
        this.authService.hideLoader();        
        this.presentToast(this.data.status.message);
        this.navCtrl.pop();
      }, 
      error => {
        this.authService.hideLoader();
        console.log(error);
      });
    }
  }

  onSignUp(){
    this.email = "";
    this.navCtrl.push(SignupPage);
  }

  presentToast(msg) {
    this.isDisabled = true;
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
      this.isDisabled = false;    
    });
    toast.present();
  }

}
