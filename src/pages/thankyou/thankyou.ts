import { Component } from '@angular/core';
import { NavController, Platform, ViewController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-thankyou',
  templateUrl: 'thankyou.html'
})
export class ThankyouPage {
  
  constructor(
    public navCtrl: NavController,
    private authService:AuthServiceProvider,
    public platform: Platform,
    public viewCtrl: ViewController) {
  }

  dismiss(){
    this.navCtrl.setRoot(HomePage);
  }
}
