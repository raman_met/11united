import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { HomePage } from '../home/home';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { PrivacyPolicyPage } from '../privacyPolicy/privacyPolicy';
import { TermsConditionPage } from '../termsCondition/termsCondition';
import { LoginPage } from '../login/login';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  fName: any = '';
  lName:any = '';
  email:any = '';
  phnNumber:any = '';
  password:any = '';
  cPassword:any = '';
  address:any = '';
  isDisabled:boolean = false;
  data: any;

  constructor(public navCtrl: NavController,
    public storage: Storage,
    public events: Events,
     public navParams: NavParams,
     public toastCtrl:ToastController,
     public authService: AuthServiceProvider) {
  }

  onSignup(){
    if(this.fName.trim() == ''){
      this.presentToast("Please Enter Your First Name");
    }else if(this.lName.trim() == ''){
      this.presentToast("Please Enter Your Last Name");
    }else if(this.email.trim() == ''){
      this.presentToast("Please Enter Your Email");
    }else if(this.phnNumber.trim() == ''){
      this.presentToast("Please Enter Your Phone Number");
    }else if(this.password.trim() == ''){
      this.presentToast("Please Enter Your Password");
    }else if(this.cPassword.trim() == ''){
      this.presentToast("Please re-enter Password");
    }else if(this.password.trim() != this.cPassword.trim()){
      this.presentToast("Password and Confirm-Password not match");
    }else{
      //this.navCtrl.setRoot(HomePage);
      //ws-registration.php
      this.authService.showLoader('Uploading signup data...');
      var body="email="+ this.email + "&firstname="+ this.fName + "&lastname="+this.lName +"&password="+this.password +"&cpassword="+this.cPassword +"&mobile="+this.phnNumber;
      this.authService.postData("ws-registration.php", body).then(result => { 
        this.data = result;
        console.log(this.data);
        this.authService.hideLoader();
        if(this.data.status.error_code == 0){
          this.storage.set("userDetails", this.data.result.details);
          this.events.publish('user:created', this.data.result.details);
          this.navCtrl.setRoot(HomePage);
        }else{
          this.presentToast(this.data.status.message)
        }
      }, 
      error => {
        this.authService.hideLoader();
        console.log(error);
      });
    }
  }

  onSignIn(){
    //this.navCtrl.push(LoginPage);
    this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'back' });
  }

  onTermsOfService(){
    this.navCtrl.push(TermsConditionPage);
  }
  onPrivacyPolicy(){
    this.navCtrl.push(PrivacyPolicyPage);
  }

  presentToast(msg) {
    this.isDisabled = true;
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom',
      dismissOnPageChange: true,
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
      this.isDisabled = false;    
    });
    toast.present();
  }

}
