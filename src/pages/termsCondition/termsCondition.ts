import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

@Component({
  selector: 'page-termsCondition',
  templateUrl: 'termsCondition.html'
})
export class TermsConditionPage {

  profile: string = "today";
  userData: any;
  data: any;
  profile_view = 0;
  wows_review = 0;
  matched = 0;
  trustedVideoUrl : any;
  htmlContent:any;

  constructor(public domSanitizer: DomSanitizer,
    public navCtrl: NavController,
    private authService:AuthServiceProvider) {
    // let url = 'http://inhouse.fitser.com/VividGoods/php/ws-terms-conditions/';
    // this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
    // console.log('trustedVideoUrl: ',this.trustedVideoUrl);
  }

  ngOnInit(){
    this.authService.showLoader('Please Wait...');
    var body="page_name=terms-and-conditions"
    this.authService.postData("ws-pagecontent.php", body).then(result => { 
      this.data = result;
      console.log(this.data);
      this.authService.hideLoader();
      if(this.data.status.error_code == 0){
        this.htmlContent = this.data.result.details.post_content
        console.log(this.htmlContent);
        //this.authService.presentToast(this.data.status.message);
      }else{
        this.authService.presentToast(this.data.status.message)
      } 
    }, 
    error => {
      this.authService.hideLoader();
      console.log(error);
    });
  }

}
