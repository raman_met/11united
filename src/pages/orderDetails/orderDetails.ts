import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

@Component({
  selector: 'page-orderDetails',
  templateUrl: 'orderDetails.html'
})
export class OrderDetailsPage {

  data: any;
  trustedVideoUrl : any;
  orderId:any;

  constructor(public domSanitizer: DomSanitizer, public navCtrl: NavController, public navparams:NavParams,
    private authService:AuthServiceProvider) {
    this.orderId = this.navparams.get('orderID');
    let url = 'https://elevenunited.fitser.com/order-detail/?order_id='+this.orderId;
    this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
    console.log('trustedVideoUrl: ',this.trustedVideoUrl);
  }

}
