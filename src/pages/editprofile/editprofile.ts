import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ToastController, ActionSheetController, Platform, Events } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { Camera } from '@ionic-native/camera';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { Storage } from '@ionic/storage';
declare var cordova: any;

/**
 * Generated class for the BooktablePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-editprofile',
  templateUrl: 'editprofile.html',
})
export class EditprofilePage {

  fname: any = "";
  lname: any = "";
  email: any = "";
  mobile: any = "";
  refid: any;
  data: any;
  lastImage: string = null;
  profile_pic: any;
  isDisabled:boolean = false;
  userDetails:any;
  userID: any;

  constructor(
    public events: Events,
    private file: File,
    public storage: Storage,
    public toastCtrl: ToastController,
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    private filePath: FilePath,
    private camera: Camera,
    public actionSheetCtrl: ActionSheetController,
    public authService: AuthServiceProvider) {
      this.getProfiledata();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditprofilePage ');
  }

  getProfiledata(){
    this.authService.showLoader('Please wait...');
    this.storage.get('userDetails').then((result) => {
      this.data = result;
      this.userID = result.ID;
      console.log("userID: ", this.userID);
      var body = "user_id=" + this.userID;
      this.authService.postData("ws-personal-info.php", body).then(result => {
        this.data = result;        
        this.userDetails = this.data.result.personal_detail[0];
        this.authService.hideLoader();
        this.email = this.userDetails.user_email;
        this.fname = this.userDetails.first_name;
        this.lname = this.userDetails.last_name;
        this.mobile = this.userDetails.mobile;
        this.profile_pic = this.userDetails.profile_image;
        this.events.publish('user:created', this.userDetails);
        console.log("userDetails: ", this.userDetails);
      },
      error => {
        this.authService.hideLoader();
        console.log(error);
      });
    });
  }

  uploadProfilepic() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            console.log("filePath: ", filePath);
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
            this.readDataFile(currentName);
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        this.readDataFile(currentName);
      }
    }, (err) => {
      console.log(err);
    });
  }

  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
      this.saveUserData(this.lastImage);
    }, error => {
      console.log("erroe", error);
    });
  }

  private readDataFile(currentName) {
    this.file.readAsDataURL(cordova.file.externalCacheDirectory, currentName).then(dataurl => {
      this.profile_pic = dataurl;
      console.log("---dataurl----" + dataurl);
    },
    (error) => {
      console.log(error);
    });
  }

  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  saveUserData(profileImage) {
    var apiFunc = 'ws-profileimage.php';
    var targetPath = this.pathForImage(profileImage);
    var options: any;
    var filename = profileImage;
    options = {
      fileKey: "profile_pic",
      fileName: filename,
      chunkedMode: false,
      mimeType: "image/jpeg", //"multipart/form-data",
      params: {
        fileKey: filename,
        user_id: this.userID
      }
    };
    console.log("options: ", options);
    this.authService.showLoader('Uploading...');
    this.authService.fileUpload(targetPath, apiFunc, options).then(result => {
      this.data = result;
      console.log("Your data: ", this.data);
      this.authService.hideLoader();
      this.getProfiledata();
    },
    error => {
      console.log(error);
      this.authService.hideLoader();
    });
  }

  presentToast(msg) {
    this.isDisabled = true;
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom',
      dismissOnPageChange: true,
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
      this.isDisabled = false;
    });
    toast.present();
  }

  onSaveprofile() {
    if (this.fname.trim() == "") {
      this.presentToast('Please enter your First name');
    } else if (this.lname.trim() == "") {
      this.presentToast('Please enter your Last name');
    } else if (this.mobile.trim() == "") {
      this.presentToast('Please enter your Mobile number');
    } else {
      //ws-update-personal-info.php
      this.authService.showLoader('Please wait...');
      var body = "user_id=" + this.userID + "&first_name="+this.fname + "&last_name="+this.lname + "&mobile="+this.mobile;
      this.authService.postData("ws-update-personal-info.php", body).then(result => {
        this.data = result;
        console.log("Update Profile : ", this.data);
        this.authService.hideLoader();
        this.getProfiledata();
      },
      error => {
        this.authService.hideLoader();
        console.log(error);
      }); 
    }
  }

}
