import { Component } from '@angular/core';
import { NavController, NavParams, Events, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-productsort',
  templateUrl: 'productsort.html'
})
export class ProductsortPage {

  isRadio1Checked:boolean = true;
  isRadio2Checked:boolean = false;

  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    public storage: Storage,
    public navParams: NavParams,) {
      this.storage.get('sort').then((result) => {
        console.log("sort: ", result);
        if(result != null && result == 'radio1'){
          this.isRadio1Checked = true;
          this.isRadio2Checked = false;
        } else if(result != null && result == 'radio2') {
          this.isRadio1Checked = false;
          this.isRadio2Checked = true;
        }
      });
  }  

  onClick(){
    this.viewCtrl.dismiss({sort:''});
  }

  onSort(val){
    if(val == 'price_low_high'){
      this.storage.set("sort", 'radio1');
    } else {
      this.storage.set("sort", 'radio2');
    }    
    this.viewCtrl.dismiss({sort:val}); 
  }

}
