import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';
import { CartPage } from '../cart/cart';
import { NewaddressPage } from '../newAddress/newaddress';
import { WishlistPage } from '../wishlist/wishlist';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { OrderDetailsPage } from '../orderDetails/orderDetails';
import * as moment from 'moment';

@Component({
  selector: 'page-orderhistory',
  templateUrl: 'orderhistory.html'
})
export class OrderhistoryPage {

  cartCount:any = 0;
  wishCount:any = 0;
  data: any;
  userID: any;
  orderData: any = [];

  constructor(public navCtrl: NavController,
    public authService: AuthServiceProvider,
    public events: Events, 
    public storage: Storage,
    public navParams: NavParams) {
    this.events.subscribe('cart:count', (eventData) => {
      this.cartCount = eventData;
    });
    this.events.subscribe('wish:count', (eventData) => {
      //console.log(eventData);
      this.wishCount = eventData;
    }); 
    this.getallData();
  }

  getallData(){   
    this.storage.get('userDetails').then((result) => {
      this.data = result;
      this.userID = result.ID;
      console.log("userID: ", this.userID);
      this.authService.showLoader('Please wait...');
      var body = "user_id=" + this.userID;
      this.authService.postData("ws-orderlist.php", body).then(result => {
        this.data = result;        
        this.orderData = this.data.result.order_list;
        console.log("Order List: ", this.orderData);
        this.authService.hideLoader();
      },
      error => {
        this.authService.hideLoader();
        console.log(error);
      }); 
    }); 
  }

  oncart(){
    this.navCtrl.push(CartPage);
  }

  onwhishlist(){
    this.navCtrl.push(WishlistPage);
  }

  onOrderDetails(val){
    this.navCtrl.push(OrderDetailsPage, {orderID: val});
  }

  formatDate(my_date){   
    return moment(my_date).format('ll');
  }

}
