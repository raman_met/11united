import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

@Component({
  selector: 'page-privacyPolicy',
  templateUrl: 'privacyPolicy.html'
})
export class PrivacyPolicyPage {

  profile: string = "today";
  userData: any;
  data: any;
  profile_view = 0;
  wows_review = 0;
  matched = 0;
  trustedVideoUrl : any;
  htmlContent:any

  constructor(public domSanitizer: DomSanitizer,
    public navCtrl: NavController,
    private authService:AuthServiceProvider) {
    // let url = 'http://inhouse.fitser.com/VividGoods/php/ws-privacy-policy/';
    // this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
    // console.log('trustedVideoUrl: ',this.trustedVideoUrl);
  }


  ngOnInit(){
    this.authService.showLoader('Please Wait...')
    var body="page_name=privacy-policy"
    this.authService.postData("ws-pagecontent.php", body).then(result => { 
      this.data = result;
      this.authService.hideLoader();
      if(this.data.status.error_code == 0){
        this.htmlContent = this.data.result.details.post_content;
        console.log(this.htmlContent);;
      } else {
        this.authService.presentToast(this.data.status.message)
      }
    }, 
    error => {
      this.authService.hideLoader();
      console.log(error);
    });
  }

}
