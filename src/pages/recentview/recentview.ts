import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ProductdetailsPage } from '../productdetails/productdetails';

@Component({
  selector: 'page-recentview',
  templateUrl: 'recentview.html'
})
export class RecentviewPage {

  products: any = [];

  constructor(
    public navCtrl: NavController,
    public storage: Storage,
  ) {
    this.storage.get('productRecentView').then((result: any[]) => {
      console.log("Recent View: ", result);
      console.log("products: ", this.products.length);
      if (result != null ) {
        this.products = this.removeDuplicity(result);
        console.log("Recent View: ", this.products);
      }
    });

  }

  removeDuplicity(datas) {
    return datas.filter((item, index, arr) => {
      const c = arr.map(item => item.id);
      return index === c.indexOf(item.id)
    })
  }

  onProductdetails(id) {
    this.navCtrl.push(ProductdetailsPage, { proID: id });
    console.log(id);
  }

}
