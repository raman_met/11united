import { Component } from '@angular/core';
import { NavController, NavParams, Events, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';
import { CartPage } from '../cart/cart';
import { NewaddressPage } from '../newAddress/newaddress';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

@Component({
  selector: 'page-wishlist',
  templateUrl: 'wishlist.html'
})

export class WishlistPage {

  userID: any;
  userDetails: any;
  data: any;
  cartCount: any = 0;
  wishData: any = [];
  alert_Ctrl: any;

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public events: Events,
    public storage: Storage,
    public navParams: NavParams,
    public authService: AuthServiceProvider, ) {
    this.events.subscribe('cart:count', (eventData) => {
      //console.log(eventData);
      this.cartCount = eventData;
    });
    this.storage.get('userDetails').then((result) => {
      this.data = result;
      this.userID = result.ID;
      console.log("userID: ", this.userID);
      this.authService.showLoader('Please wait...');
      this.getallData();
    });
  }

  getallData() {
    var body = "user_id=" + this.userID;
    this.authService.postData("ws-wishlist.php", body).then(result => {
      this.data = result;
      console.log("Wish List: ", this.data);
      this.wishData = this.data.result.list;
      this.authService.hideLoader();
    },
      error => {
        this.authService.hideLoader();
        console.log(error);
      });
  }

  onaddcart(prodID) {
    this.authService.showLoader('Please wait...');
    var body = "user_id=" + this.userID + "&id=" + prodID + "&qty=1";
    this.authService.postData("ws-productaddtocart.php", body).then(result => {
      this.data = result;
      console.log("addtocart : ", this.data);
      this.authService.hideLoader();
      this.navCtrl.push(CartPage);
    },
      error => {
        this.authService.hideLoader();
        console.log(error);
      });
  }

  onDelete(wishID) {
    this.alert_Ctrl = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Are you sure you want to delete this item from Shortlist?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            setTimeout(() => {
              this.alert_Ctrl.dismiss();
            }, 1000);
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.authService.showLoader('Please wait...');
            var body = "wishlist_id=" + wishID;
            this.authService.postData("ws-removefromwishlist.php", body).then(result => {
              this.data = result;
              console.log("addtocart : ", this.data);
              this.authService.hideLoader();
              this.getallData();
            },
              error => {
                this.authService.hideLoader();
                console.log(error);
              });
          }
        }
      ]
    });
    this.alert_Ctrl.present();
  }

  oncart() {
    this.navCtrl.push(CartPage);
  }

}
