import { Component } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ProfilePage } from '../profile/profile';
import { ShippingaddressbookPage } from '../shippingaddressbook/shippingaddressbook';
import { BillingaddressbookPage } from '../billingaddressbook/billingaddressbook';
import { WishlistPage } from '../wishlist/wishlist';
import { RecentviewPage } from '../recentview/recentview';
import { CartPage } from '../cart/cart';
import { OrderhistoryPage } from '../orderhistory/orderhistory';
import { MyreviewsPage } from '../myreviews/myreviews';
import { QueriesPage } from '../queries/queries';
import { MyqueriesPage } from '../myqueries/myqueries';

@Component({
  selector: 'page-myaccount',
  templateUrl: 'myaccount.html'
})
export class MyaccountPage {
  showMyOrder: boolean = false;
  showMyProfile: boolean = false;
  showMyAddress: boolean = false;
  cartCount:any = 0;
  wishCount:any = 0;

  constructor(public domSanitizer: DomSanitizer,
    public events: Events,
    public navCtrl: NavController,
    private authService: AuthServiceProvider) {
      this.events.subscribe('cart:count', (eventData) => {
        console.log(eventData);
        this.cartCount = eventData;
      }); 
      this.events.subscribe('wish:count', (eventData) => {
        //console.log(eventData);
        this.wishCount = eventData;
      });
  }

  ngOnInit() {

  }

  showMyordersItems() {
    if (this.showMyOrder == false) {
      this.showMyOrder = true;
    } else {
      this.showMyOrder = false;
    }
  }

  showMyProfileItems() {
    if (this.showMyProfile == false) {
      this.showMyProfile = true;
    } else {
      this.showMyProfile = false;
    }
  }

  onMyProfile(){
    this.navCtrl.push(ProfilePage);
  }

  onAddressBook(){
    //this.navCtrl.push(AddressbookPage);
    if (this.showMyAddress == false) {
      this.showMyAddress = true;
    } else {
      this.showMyAddress = false;
    }
  }

  onShipping(){
    this.navCtrl.push(ShippingaddressbookPage);
  }

  onBilling(){
    this.navCtrl.push(BillingaddressbookPage);
  }

  onReviews(){
    this.navCtrl.push(MyreviewsPage);
  }

  onMyorders(){
    console.log("do somthimg");
  }

  onRecentlyView(){
    this.navCtrl.push(RecentviewPage);
  }

  oncart(){
    this.navCtrl.push(CartPage);
  }

  onWishlist(){
    this.navCtrl.push(WishlistPage);
  }

  onOrderhistory() {
    this.navCtrl.push(OrderhistoryPage);
  }

  onQueries(){
    this.navCtrl.push(QueriesPage);
  }

  onMyQueries(){
    this.navCtrl.push(MyqueriesPage);
  }

}
