import { Component } from '@angular/core';
import { NavController, NavParams, Keyboard, ToastController, Events } from 'ionic-angular';
import { HomePage } from '../home/home';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { PrivacyPolicyPage } from '../privacyPolicy/privacyPolicy';
import { TermsConditionPage } from '../termsCondition/termsCondition';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { CartPage } from '../cart/cart';
import { WishlistPage } from '../wishlist/wishlist';

@Component({
  selector: 'page-newaddress',
  templateUrl: 'newaddress.html'
})
export class NewaddressPage {
  landmark: any = '';
  postcode: any = '';
  city: any = '';
  state: any;
  phone: any = '';
  statesData: any = [];
  stateAlertOpts: { title: string };
  address_type: any = 'Home';
  addressType: any;
  isDisabled:boolean = false;
  data: any;
  userID: any;
  cartCount:any = 0;
  wishCount:any = 0;
  same_address: boolean = false;

  constructor(public navCtrl: NavController,
    public events: Events,
    public toastCtrl:ToastController,
    public navParams: NavParams,
    public http: HttpClient,
    public storage: Storage,
    public keyboard: Keyboard,
    public authService: AuthServiceProvider) {
    this.addressType = this.navParams.get('addressType');
    this.landmark = this.navParams.get('landmark');
    this.postcode = this.navParams.get('postcode');
    this.city = this.navParams.get('city');
    this.state = this.navParams.get('state');
    this.phone = this.navParams.get('phone');
    this.address_type = this.navParams.get('address_type');
    console.log(this.navParams.get('address_type'));
    this.stateAlertOpts = { title: 'Select State' }
    this.http.get('assets/xml/states.json').subscribe(data => {
      this.statesData = data;
      console.log(this.statesData);
    }, error => {
      console.log(error);
    });
    this.storage.get('userDetails').then((result) => {
      this.data = result;
      this.userID = result.ID;
      console.log("userID: ", this.userID);
    }); 
    this.events.subscribe('cart:count', (eventData) => {
      console.log(eventData);
      this.cartCount = eventData;
    });
    this.events.subscribe('wish:count', (eventData) => {
      //console.log(eventData);
      this.wishCount = eventData;
    });
  }

  onAddressType(value) {
    //this.address_type = value;
    //console.log(this.address_type);
  }

  stateChange() {
    if (this.same_address == true) {
      
    } else {

    }
  }

  onSave() {
    if (this.landmark.trim() == '') {
      this.presentToast("Please Enter Landmark");
    } else if (this.postcode.trim() == '') {
      this.presentToast("Please Enter Postcode");
    } else if (this.city.trim() == '') {
      this.presentToast("Please Enter City");
    } else if (this.state == null) {
      this.presentToast("Please select State");
    } else if (this.phone.trim() == '') {
      this.presentToast("Please Enter Mobile");
    } else {
      this.authService.showLoader('Please wait...'); 
      if(this.addressType == 'Shipping'){
        var apiName = 'ws-add-shipping-address.php';
        var body="user_id="+ this.userID + "&shipping_landmark="+ this.landmark + "&shipping_city="+this.city +"&shipping_state="+this.state +"&shipping_phone="+this.phone +"&shipping_postcode="+this.postcode +"&shipping_address_type="+this.address_type;
      } else {
        var apiName = 'ws-add-address.php';
        var body="user_id="+ this.userID + "&billing_landmark="+ this.landmark + "&billing_city="+this.city +"&billing_state="+this.state +"&billing_phone="+this.phone +"&billing_postcode="+this.postcode +"&billing_address_type="+this.address_type;
      }
      this.authService.postData(apiName, body).then(result => { 
        this.data = result;
        console.log(this.data);
        this.authService.hideLoader();
        if(this.addressType == 'Shipping'){
          this.events.publish('shipping_address:added', this.data);
        } else {
          this.events.publish('billing_address:added', this.data);
        } 
        this.navCtrl.pop();       
      }, 
      error => {
        this.authService.hideLoader();
        console.log(error);
      });
    }
  }

  presentToast(msg) {
    this.isDisabled = true;
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
      this.isDisabled = false;    
    });
    toast.present();
  }

  oncart(){
    this.navCtrl.push(CartPage);
  }

  onwhishlist(){
    this.navCtrl.push(WishlistPage);
  }

}
