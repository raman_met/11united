import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { CartPage } from '../cart/cart';
import { WishlistPage } from '../wishlist/wishlist';

@Component({
  selector: 'page-productreview',
  templateUrl: 'productreview.html'
})
export class ProductreviewPage {

  empty: any;
  data: any;
  prodID: any;
  reviewData: any = [];
  cartCount:any = 0;
  wishCount:any = 0;

  constructor(
    public navCtrl: NavController,
    public events: Events,
    public authService: AuthServiceProvider,
    public navParams: NavParams,
    public storage: Storage,
  ) {
    this.prodID = this.navParams.get('prodID');
    this.events.subscribe('cart:count', (eventData) => {
      //console.log(eventData);
      this.cartCount = eventData;
    }); 
    this.events.subscribe('wish:count', (eventData) => {
      //console.log(eventData);
      this.wishCount = eventData;
    });
    console.log("prodID: ", this.prodID);
    this.myReviews();   
  }


  myReviews() {
    this.authService.showLoader('Please wait...');
    var body = "id="+this.prodID;
    this.authService.postData("ws-product-review-list.php", body).then(result => {
      this.data = result;        
      this.reviewData = this.data.result.review_list;
      console.log("review: ", this.data);
      this.authService.hideLoader();
    },
    error => {
      this.authService.hideLoader();
      console.log(error);
    });
  }

  formatdate(review_date){
    var newdate;
    newdate = review_date.split(' ');
    return newdate[0];
  }

  oncart(){
    this.navCtrl.push(CartPage);
  }

  onwhishlist(){
    this.navCtrl.push(WishlistPage);
  }

}
