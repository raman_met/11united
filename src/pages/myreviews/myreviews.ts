import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { CartPage } from '../cart/cart';
import { WishlistPage } from '../wishlist/wishlist';
import * as moment from 'moment';

@Component({
  selector: 'page-myreviews',
  templateUrl: 'myreviews.html'
})
export class MyreviewsPage {

  empty: any;
  data: any;
  reviewData: any = [];
  cartCount:any = 0;
  wishCount:any = 0;
  userID: any;

  constructor(
    public navCtrl: NavController,
    public events: Events,
    public authService: AuthServiceProvider,
    public navParams: NavParams,
    public storage: Storage,
  ) {
    this.events.subscribe('cart:count', (eventData) => {
      //console.log(eventData);
      this.cartCount = eventData;
    }); 
    this.events.subscribe('wish:count', (eventData) => {
      //console.log(eventData);
      this.wishCount = eventData;
    });
    this.myReviews();   
  }


  myReviews() {
    this.storage.get('userDetails').then((result) => {
      this.data = result;
      this.userID = result.ID;
      console.log("userID: ", this.userID);
      this.authService.showLoader('Please wait...');
      var body = "user_id="+this.userID;
      this.authService.postData("my-review-list.php", body).then(result => {
        this.data = result;        
        this.reviewData = this.data.result.review_list;
        console.log("review: ", this.data);
        this.authService.hideLoader();
      },
      error => {
        this.authService.hideLoader();
        console.log(error);
      });
    });
  }

  formatdate(review_date){
    return moment(review_date).format('ll');
  }

  oncart(){
    this.navCtrl.push(CartPage);
  }

  onwhishlist(){
    this.navCtrl.push(WishlistPage);
  }

}
