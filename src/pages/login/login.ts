import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { HomePage } from '../home/home';
import { SignupPage } from '../signup/signup';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { Storage } from '@ionic/storage';
import { ForgetpasswordPage } from '../forgetpassword/forgetpassword';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  email: any = '';
  password: any = '';
  data: any;
  isDisabled: boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public authService: AuthServiceProvider,
    public storage: Storage,
    public events: Events,
  ) {

  }

  onLogin() {
    /* var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var passRegex = /^(?=\D*\d)(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,30}$/;
    var passRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}$/
    if(!passRegex.test(this.password)) {
      console.log('Invalid password');
    } else {
      console.log('Valid password');
    } */
    if (this.email.trim() == '') {
      this.presentToast("Please Enter Email")
    } else if (this.password.trim() == '') {
      this.presentToast("Please Enter Password")
    } else {
      this.authService.showLoader('Authenticating...');
      var body = "email=" + this.email + "&password=" + this.password;
      this.authService.postData("ws-login.php", body).then(result => {
        this.data = result;
        console.log("login: ", this.data);
        this.authService.hideLoader();
        if (this.data.status.error_code == 0) {
          //this.presentToast(this.data.status.message);
          this.storage.set("userDetails", this.data.result.details);
          this.events.publish('user:created', this.data.result.details);
          this.navCtrl.setRoot(HomePage);
        } else {
          this.presentToast(this.data.status.message);
        }
      },
        error => {
          this.authService.hideLoader();
          console.log(error);
        });
    }
  }

  onSignUp() {
    //this.navCtrl.setRoot(SignupPage);
    this.navCtrl.setRoot(SignupPage, {}, { animate: true, direction: 'forward' });
  }

  forgetpass() {
    this.navCtrl.push(ForgetpasswordPage);
  }

  facebookLogin() {

  }

  presentToast(msg) {
    this.isDisabled = true;
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom',
      dismissOnPageChange: true,
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
      this.isDisabled = false;
    });
    toast.present();
  }

}
