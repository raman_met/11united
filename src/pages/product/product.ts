import { Component } from '@angular/core';
import { NavController, NavParams, Events, ActionSheetController, ModalController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';
import { ProductdetailsPage } from '../productdetails/productdetails';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { WishlistPage } from '../wishlist/wishlist';
import { CartPage } from '../cart/cart';
import { ProductsortPage } from '../productsort/productsort';
import { ProductfilterPage } from '../productfilter/productfilter';

@Component({
  selector: 'page-product',
  templateUrl: 'product.html'
})
export class ProductPage {

  cartCount: any = 0;
  data: any;
  search: any = '';
  productData: any = [];
  pageNum: any = 1;
  totalPage: any;
  userID: any;
  userDetails: any;
  catID: any;
  catName: any;
  wishCount: any = 0;
  sortValue: any = 'price_low_high';
  size_id: any = '';
  color_id: any = '';
  minp: any = '';
  maxp: any = '';

  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    public events: Events,
    public storage: Storage,
    public authService: AuthServiceProvider,
    public navParams: NavParams) {
    this.events.subscribe('cart:count', (eventData) => {
      //console.log(eventData);
      this.cartCount = eventData;
    });
    this.events.subscribe('wish:count', (eventData) => {
      //console.log(eventData);
      this.wishCount = eventData;
    });

  }

  onLogin() {
    this.navCtrl.push(ProductdetailsPage);
  }

  ionViewWillEnter() {
    this.catID = this.navParams.get('catID');
    this.catName = this.navParams.get('catName');
    console.log("catID: ", this.catID);
    this.productData = [];
    this.onRefinesearch(this.pageNum, this.catID, this.sortValue, this.size_id, this.color_id, this.minp, this.maxp);
  }

  ionViewWillLeave() {
    console.log("ionViewWillLeave: ");
    this.storage.remove('sort');
    this.storage.remove("price_list");
  }

  onRefinesearch(page_num, cat_id, sort_value, size_id, color_id, minp, maxp) {
    this.storage.get('userDetails').then((result) => {
      this.data = result;
      this.userID = result.ID;
      console.log("userID: ", this.userID);
      this.authService.showLoader('Please wait...');
      var body = "product_name=" + "&page_number=" + page_num + "&cat_id=" + cat_id 
      + "&user_id=" + this.userID + "&sort_filter=" + sort_value
      + "&size_id=" + size_id + "&color_id=" + color_id + "&minp=" + minp + "&maxp=" + maxp;
      this.authService.postData("ws-product-sort.php", body).then(result => {
        this.data = result;
        console.log("Search Details: ", this.data);
        this.totalPage = this.data.result.total_page;
        for (let i = 0; i < this.data.result.product_list.length; i++) {
          this.productData.push(this.data.result.product_list[i]);
        }
        this.authService.hideLoader();
      },
        error => {
          this.authService.hideLoader();
          console.log(error);
        });
    });
  }

  doInfinite(infiniteScroll) {
    this.pageNum += 1;
    this.onRefinesearch(this.pageNum, this.catID, this.sortValue, this.size_id, this.color_id, this.minp, this.maxp);
  }

  addtowishlist(prodID) {
    this.storage.get('userDetails').then((result) => {
      this.data = result;
      this.userID = result.ID;
      console.log("userID: ", this.userID);
      this.authService.showLoader('Please wait...');
      var body = "user_id=" + this.userID + "&product_ids=" + prodID;
      this.authService.postData("ws-addtowishlist.php", body).then(result => {
        this.data = result;
        console.log("addtowishlist : ", this.data);
        this.authService.hideLoader();
        this.navCtrl.push(WishlistPage);
      },
        error => {
          this.authService.hideLoader();
          console.log(error);
        });
    });
  }

  addtocart(prodID) {
    this.storage.get('userDetails').then((result) => {
      this.data = result;
      this.userID = result.ID;
      console.log("userID: ", this.userID);
      this.authService.showLoader('Please wait...');
      var body = "user_id=" + this.userID + "&id=" + prodID + "&qty=1";
      this.authService.postData("ws-productaddtocart.php", body).then(result => {
        this.data = result;
        console.log("addtocart : ", this.data);
        this.authService.hideLoader();
        this.navCtrl.push(CartPage);
      },
        error => {
          this.authService.hideLoader();
          console.log(error);
        });
    });
  }

  openSortPage() {
    let opts: any = {
      showBackdrop: true,
      enableBackdropDismiss: true,
      EnterAnimation: 'slide-in',
      LeaveAnimation: 'slide-out',
    }
    const modal = this.modalCtrl.create(ProductsortPage, {}, opts);
    modal.onDidDismiss(data => {
      console.log(data.sort);
      this.sortValue = data.sort;
      if (this.sortValue != '') {
        this.productData = [];
        this.onRefinesearch(this.pageNum, this.catID, this.sortValue, this.size_id, this.color_id, this.minp, this.maxp);
      }
    });
    modal.present();
  }

  openFilterPage() {
    //this.navCtrl.push(ProductfilterPage);
    let opts: any = {
      showBackdrop: true,
      enableBackdropDismiss: true
    }
    const modal = this.modalCtrl.create(ProductfilterPage, {}, opts);
    modal.onDidDismiss(data => {
      console.log(data);
      this.color_id = data.colorID;
      this.size_id = data.sizeID;
      this.minp = data.minprice;
      this.maxp = data.maxprice;
      if (this.minp != '') {
        this.productData = [];
        this.onRefinesearch(this.pageNum, this.catID, "", this.size_id, this.color_id, this.minp, this.maxp);
      }
    });
    modal.present();
  }

  onProductdetails(pro_id) {
    this.navCtrl.push(ProductdetailsPage, { proID: pro_id });
  }

  oncart() {
    this.navCtrl.push(CartPage);
  }

  onwhishlist() {
    this.navCtrl.push(WishlistPage);
  }

}
