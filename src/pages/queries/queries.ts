import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ToastController, Keyboard, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

@Component({
  selector: 'page-queries',
  templateUrl: 'queries.html'
})
export class QueriesPage {
  name: any = '';
  email: any = '';
  mobile: any = '';
  subject: any = '';
  message: any = '';
  data: any;
  isDisabled: boolean = false;
  haderName: any;
  userID: any;

  constructor(public navCtrl: NavController,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public storage: Storage,
    private authService: AuthServiceProvider,
    public keyboard: Keyboard) {

  }

  presentToast(msg) {
    this.isDisabled = true;
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom',
      dismissOnPageChange: true,
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
      this.isDisabled = false;
    });
    toast.present();
  }

  onSubmit() {
    this.storage.get('userDetails').then((result) => {
      this.data = result;
      this.userID = result.ID;
      console.log("userID: ", this.userID);
      if (this.name.trim() == "") {
        this.presentToast("Please Enter Your Name")
      } else if (this.email.trim() == "") {
        this.presentToast("Please Enter Your Email ID")
      } else if (this.mobile.trim() == "") {
        this.presentToast("Please Enter Your Mobile number")
      } else if (this.subject.trim() == "") {
        this.presentToast("Please Enter Subject")
      } else if (this.message.trim() == "") {
        this.presentToast("Please Enter Message")
      } else {
        this.authService.showLoader('Please Wait...');
        var body =
          "user_id=" + this.userID +
          "&contact_name=" + this.name +
          "&contact_email=" + this.email +
          "&contact_number=" + this.mobile +
          "&contact_subject=" + this.subject +
          "&contact_msg=" + this.message;
        this.authService.postData("ws-query.php", body).then(result => {
          console.log(result);
          this.data = result;
          this.authService.hideLoader();
          this.name = "";
          this.email = "";
          this.mobile = "";
          this.subject = "";
          this.message = "";
          this.presentToast(this.data.status.message);
        },
          error => {
            this.authService.hideLoader();
            console.log(error);
          });
      }
    })
  }
}
