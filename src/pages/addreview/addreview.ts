import { Component } from '@angular/core';
import { NavController, ToastController, Keyboard, NavParams, Events } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-addreview',
  templateUrl: 'addreview.html'
})
export class AddreviewPage {
  name: any = "";
  email: any = "";
  message: any = "";
  data: any;
  productId: any;
  userdata: any;
  userID: any;
  show_header: boolean = true;
  myRating: any = '3';

  constructor(public navCtrl: NavController,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public storage: Storage,
    private events: Events,
    private authService: AuthServiceProvider,
    public keyboard: Keyboard) {
    this.productId = this.navParams.get('prodID');
    console.log(this.productId);
  }

  logRatingChange(rating) {
    console.log("changed rating: ", rating);
    this.myRating = rating;
  }

  onSubmit() {
    this.storage.get('userDetails').then((result) => {
      this.data = result;
      this.userID = result.ID;
      if (this.name.trim() == '') {
        this.authService.presentToast("Please Enter Your Name")
      } else if (this.email.trim() == "") {
        this.authService.presentToast("Please Enter Your Email")
      } else if (this.message.trim() == "") {
        this.authService.presentToast("Please Enter Your Review")
      } else {
        this.authService.showLoader('Please Wait...');
        var body =
          "user_id=" + this.userID +
          "&id=" + this.productId +
          "&customer_name=" + this.name +
          "&customer_email=" + this.email +
          "&customer_message=" + this.message +
          "&rating=" + this.myRating;
        this.authService.postData("ws-product-review.php", body).then(result => {
          console.log(result);
          this.data = result
          this.authService.hideLoader();
          if (this.data.status.error_code == 0) {
            this.name = " ";
            this.email = " ";
            this.message = "";
            this.authService.presentToast(this.data.status.message);
            console.log(this.data);
            this.events.publish('review:added', this.data);
          } else {
            this.authService.presentToast(this.data.status.message);
          }
        },
          error => {
            this.authService.hideLoader();
            console.log(error);
          });
      }
    });
  }

}
