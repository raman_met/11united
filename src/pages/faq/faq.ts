import { Component } from '@angular/core';
import { NavController, PopoverController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html'
})
export class FaqPage {
  faqList:any;
  data:any;

  constructor(public navCtrl: NavController,
    public popoverCtrl: PopoverController,
    private authService:AuthServiceProvider,
    ) {
      this.faqListData();
  }

  faqListData(){
    this.authService.showLoader('Loading...');
    this.authService.getData("ws-faq.php").then(result => { 
      this.data = result;
      this.authService.hideLoader();
      this.faqList = this.data.result.faqs;
      console.log(this.faqList);
    }, 
    error => {
      this.authService.hideLoader();
      console.log(error);
    });
  }

}
