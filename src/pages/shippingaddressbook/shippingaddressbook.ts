import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { HomePage } from '../home/home';
import { CartPage } from '../cart/cart';
import { Storage } from '@ionic/storage';
import { NewaddressPage } from '../newAddress/newaddress';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { WishlistPage } from '../wishlist/wishlist';

@Component({
  selector: 'page-shippingaddressbook',
  templateUrl: 'shippingaddressbook.html'
})
export class ShippingaddressbookPage {

  data: any;
  userID: any;
  fname: any;
  lname: any;
  landmark: any = '';
  postcode: any;
  city: any;
  state: any;
  phone: any;
  address_type: any;
  cartCount:any = 0;
  wishCount:any = 0;

  constructor(public navCtrl: NavController,
    public authService: AuthServiceProvider,
    public storage: Storage,
    public events: Events,
    public navParams: NavParams) {
      this.storage.get('userDetails').then((result) => {
        this.data = result;
        console.log("userDetails: ", this.data);
        this.userID = this.data.ID;
        this.getAddressDetails();
      });
      events.subscribe('shipping_address:added', (data) => {
        this.getAddressDetails();
      });
      this.events.subscribe('cart:count', (eventData) => {
        console.log(eventData);
        this.cartCount = eventData;
      });
      this.events.subscribe('wish:count', (eventData) => {
        //console.log(eventData);
        this.wishCount = eventData;
      });  
  }

  getAddressDetails(){
    this.authService.showLoader('Please wait...');
    var body = "user_id=" + this.userID;
    this.authService.postData("ws-show-shipping-address.php", body).then(result => {
      this.data = result;
      console.log("address: ", this.data);
      this.landmark = this.data.shipping_address;
      this.address_type = this.data.shipping_address_type;
      this.city = this.data.shipping_city;
      this.fname = this.data.shipping_first_name;
      this.lname = this.data.shipping_last_name;
      this.phone = this.data.shipping_phone;
      this.postcode = this.data.shipping_postcode;
      this.state = this.data.shipping_state;
      this.authService.hideLoader();
    },
    error => {
      this.authService.hideLoader();
      console.log(error);
    });
  }

  onNewAddressBook(value){
    console.log(value);
    this.navCtrl.push(NewaddressPage, {addressType: value, landmark: this.landmark, address_type: this.address_type, city: this.city, phone: this.phone, postcode: this.postcode, state: this.state});
  }

  oncart(){
    this.navCtrl.push(CartPage);
  }

  onwhishlist(){
    this.navCtrl.push(WishlistPage);
  }

}
