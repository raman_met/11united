import { Component } from '@angular/core';
import { NavController, PopoverController, Events, ViewController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-nointernet',
  templateUrl: 'nointernet.html'
})
export class NointernetPage {
  faqList: any;
  data: any;
  show_header: boolean = true;

  constructor(public navCtrl: NavController,
    public popoverCtrl: PopoverController,
    public events: Events,
    public viewCirl: ViewController,
    private authService: AuthServiceProvider,
  ) {
    this.events.subscribe('network:online', () => {
      this.viewCirl.dismiss();       
    }); 

  }

  ngOnInit() {
  }


}
