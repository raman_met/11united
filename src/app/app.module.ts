import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { CatagoriesPage } from '../pages/catagories/catagories';
import { ProductPage } from '../pages/product/product';
import { ProductdetailsPage } from '../pages/productdetails/productdetails';
import { FaqPage } from '../pages/faq/faq';
import { AboutUsPage } from '../pages/aboutUs/aboutUs';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { HttpModule } from '@angular/http';
import { PrivacyPolicyPage } from '../pages/privacyPolicy/privacyPolicy';
import { TermsConditionPage } from '../pages/termsCondition/termsCondition';
import { CartPage } from '../pages/cart/cart';
import { MyaccountPage } from '../pages/myaccount/myaccount';
import { ProfilePage } from '../pages/profile/profile';
import { NewaddressPage } from '../pages/newAddress/newaddress';
import { WishlistPage } from '../pages/wishlist/wishlist';
import { OrderhistoryPage } from '../pages/orderhistory/orderhistory';
import { NetworkServiceProvider } from '../providers/network-service/network-service';
import { Network } from '@ionic-native/network';
import { IonicStorageModule } from '@ionic/storage';
import { ForgetpasswordPage } from '../pages/forgetpassword/forgetpassword';
import { ChangepasswordPage } from '../pages/changepassword/changepassword';
import { EditprofilePage } from '../pages/editprofile/editprofile';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { FileTransfer } from '@ionic-native/file-transfer';
import { GlobalvarsProvider } from '../providers/globalvars/globalvars';
import { HttpClientModule } from '@angular/common/http';
import { BillingaddressbookPage } from '../pages/billingaddressbook/billingaddressbook';
import { ShippingaddressbookPage } from '../pages/shippingaddressbook/shippingaddressbook';
import { ProductsortPage } from '../pages/productsort/productsort';
import { SearchPage } from '../pages/search/search';
import { RecentviewPage } from '../pages/recentview/recentview';
import { AddreviewPage } from '../pages/addreview/addreview';
import { StarRatingModule } from 'ionic3-star-rating';
import { ProductreviewPage } from '../pages/productreview/productreview';
import { ProductfilterPage } from '../pages/productfilter/productfilter';
import { SocialSharing } from '@ionic-native/social-sharing';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { MyreviewsPage } from '../pages/myreviews/myreviews';
import { QueriesPage } from '../pages/queries/queries';
import { ThankyouPage } from '../pages/thankyou/thankyou';
import { OrderDetailsPage } from '../pages/orderDetails/orderDetails';
import { OrderByPipe } from '../pipes/orderby.pipe';
import { NointernetPage } from '../pages/nointernet/nointernet';
import { MyqueriesPage } from '../pages/myqueries/myqueries';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    ForgetpasswordPage,
    ChangepasswordPage,
    SignupPage,
    CatagoriesPage,
    ProductPage,
    ProductdetailsPage,
    EditprofilePage,
    FaqPage,
    AboutUsPage,
    PrivacyPolicyPage,
    TermsConditionPage,
    CartPage,
    MyaccountPage,
    ProfilePage,
    BillingaddressbookPage,
    ShippingaddressbookPage,
    NewaddressPage,
    WishlistPage,
    OrderhistoryPage,
    ProductsortPage,
    ProductfilterPage,
    SearchPage,
    RecentviewPage,
    AddreviewPage,
    ProductreviewPage,
    MyreviewsPage,
    QueriesPage,
    ThankyouPage,
    OrderDetailsPage,
    OrderByPipe,
    NointernetPage,
    MyqueriesPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    StarRatingModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    ForgetpasswordPage,
    ChangepasswordPage,
    SignupPage,
    CatagoriesPage,
    ProductPage,
    ProductdetailsPage,
    EditprofilePage,
    FaqPage,
    AboutUsPage,
    PrivacyPolicyPage,
    TermsConditionPage,
    CartPage,
    MyaccountPage,
    ProfilePage,
    BillingaddressbookPage,
    ShippingaddressbookPage,
    NewaddressPage,
    WishlistPage,
    OrderhistoryPage,
    ProductsortPage,
    ProductfilterPage,
    SearchPage,
    RecentviewPage,
    AddreviewPage,
    ProductreviewPage,
    MyreviewsPage,
    QueriesPage,
    ThankyouPage,
    OrderDetailsPage,
    NointernetPage,
    MyqueriesPage
  ],
  providers: [
    Camera,
    File,
    FilePath,
    FileTransfer,
    StatusBar,
    SplashScreen,
    Network,
    PayPal,
    SocialSharing,
    AuthServiceProvider,
    NetworkServiceProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GlobalvarsProvider
  ]
})
export class AppModule {}
