import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, AlertController, ToastController, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { FaqPage } from '../pages/faq/faq';
import { AboutUsPage } from '../pages/aboutUs/aboutUs';
import { MyaccountPage } from '../pages/myaccount/myaccount';
import { WishlistPage } from '../pages/wishlist/wishlist';
import { OrderhistoryPage } from '../pages/orderhistory/orderhistory';
import { ProductPage } from '../pages/product/product';
import { NetworkServiceProvider } from '../providers/network-service/network-service';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { NointernetPage } from '../pages/nointernet/nointernet';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {

  @ViewChild(Nav) nav: Nav;
  // rootPage: any = LoginPage;
  rootPage: any;
  alert_ctrl: any;
  data: any;
  name: any;
  profile_pic: any;
  categoryData: any = [];
  counter = 0;
  modal: any;

  constructor(public platform: Platform,
    public events: Events,
    public storage: Storage,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public authService: AuthServiceProvider,
    public networkProvider: NetworkServiceProvider, ) {
    events.subscribe('user:created', (user) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      console.log('Welcome', user);
      this.storage.set("userDetails", user);
      this.name = user.first_name +' '+ user.last_name;
      this.profile_pic = user.user_url;
    });
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.statusBar.backgroundColorByHexString('#ffffff');
      this.networkProvider.initializeNetworkEvents();
      // Offline event
      this.events.subscribe('network:offline', () => {
        //alert('network:offline ==> ' + this.network.type);
        this.modal = this.modalCtrl.create(NointernetPage, undefined, { cssClass: "modal-fullscreen" });
        this.modal.present();
      });
      // Online event
      this.events.subscribe('network:online', () => {
        /* if (this.alert_ctrl != null) {
          this.alert_ctrl.dismiss();
        } */
      });
      this.storage.get('userDetails').then((result) => {
        this.data = result;
        console.log("userDetails: ", this.data);
        if (this.data != null) {
          this.name = this.data.first_name +' '+ this.data.last_name;          
          this.profile_pic = this.data.user_url;
          this.rootPage = HomePage;
        } else {
          this.rootPage = LoginPage;
        }
      });
      this.showAllCategory();
      this.platform.registerBackButtonAction(() => {
        if (this.counter == 0) {
          this.counter++;
          this.presentToast();
          setTimeout(() => { this.counter = 0 }, 3000)
        } else {
          // console.log("exitapp");
          this.platform.exitApp();
        }
      }, 0)
    });
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Press again to exit",
      duration: 3000,
      position: "bottom"
    });
    toast.present();
  }

  showCartCount() {
    this.storage.get('userDetails').then((result) => {
      this.data = result;
      if (this.data != null) {
        var body = "user_id=" + this.data.ID;
        this.authService.postData("ws-cartcount.php", body).then(result => {
          this.data = result;
          //console.log("cartcount: ", this.data.wishlist_count);
          this.events.publish('cart:count', this.data.cart_item_count);
          this.events.publish('wish:count', this.data.wishlist_count);
        },
          error => {
            this.authService.presentToast(error);
          });
      }
    });
  }

  showAllCategory() {
    //this.authService.showLoader('Please wait...');    
    this.authService.getData("ws-categorylist.php").then(result => {
      this.data = result;
      this.categoryData = this.data.result.list;
      //this.authService.hideLoader();
      console.log("categoryData: ", this.categoryData);
      setInterval(() => {
        this.showCartCount();
      }, 1000);
    },
      error => {
        //this.authService.hideLoader();
        this.authService.presentToast(error);
      });
  }
  /* ws-categorylist.php */

  showConfirm() {
    this.alert_ctrl = this.alertCtrl.create({
      title: 'No Internet',
      message: 'This application requires network connection. Please connect with Internet and try again',
      buttons: [
        {
          text: 'Exit App',
          handler: () => {
            this.platform.exitApp();
          }
        }
      ]
    });
    this.alert_ctrl.present();
  }

  onProductList(catID, catName) {
    this.nav.push(ProductPage, { catID: catID, catName: catName });
  }

  onMyAccount() {
    this.nav.setRoot(MyaccountPage);
  }

  onFAQ() {
    this.nav.setRoot(FaqPage);
  }

  onWishlist() {
    this.nav.setRoot(WishlistPage);
  }

  onOrderhistory() {
    this.nav.setRoot(OrderhistoryPage);
  }

  onAboutus() {
    this.nav.setRoot(AboutUsPage);
  }

  onHome() {
    this.nav.setRoot(HomePage, {}, { animate: true, direction: 'back' });
  }

  onLogout() {
    this.authService.showLoader('Logging out...');
    setTimeout(() => {
      this.storage.clear();
      this.authService.hideLoader();
      this.nav.setRoot(LoginPage, {}, { animate: true, direction: 'back' });
    }, 3000);
  }

}



