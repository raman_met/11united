import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import 'rxjs/add/operator/map';

let apiUrl = 'https://elevenunited.fitser.com/wp-content/themes/twentyseventeen/API/';

@Injectable()
export class AuthServiceProvider {
  
  loading: any;
  isDisabled: boolean = false;

  constructor(
    public http: Http,
    private transfer: FileTransfer,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    ) {
      
    }

  postData(apiName,data) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      this.http.post(apiUrl+apiName, data, {headers: headers})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          this.handleError(err);
        });
    });
  }

  getData(apiName) {
    return new Promise((resolve, reject) => {
      this.http.get(apiUrl+apiName)
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          this.handleError(err);
        });
    });
  }

  handleError(err){
    console.log("API Error Status: ", err.status);
  }

  fileUpload(targetPath, apiFunc, options){
    let favoritesURL = apiUrl + apiFunc;
    const fileTransfer: FileTransferObject = this.transfer.create();
    return fileTransfer.upload(targetPath, favoritesURL, options)
          .then(res => res.response)
          .catch(this.loading.dismiss().catch(() => { }));
  }

  showLoader(text) {
    this.loading = this.loadingCtrl.create({
      content: text
    });  
    this.loading.present();
  }

  hideLoader() {
    if(this.loading != null){
      this.loading.dismiss().catch(() => { });
    }    
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }

}